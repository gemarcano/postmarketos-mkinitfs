// Copyright 2022 Clayton Craft <clayton@craftyguy.net>
// SPDX-License-Identifier: GPL-3.0-or-later

package archive

import (
	"reflect"
	"testing"

	"github.com/cavaliercoder/go-cpio"
)

func TestArchiveItemsAdd(t *testing.T) {
	subtests := []struct {
		name     string
		inItems  []archiveItem
		inItem   archiveItem
		expected []archiveItem
	}{
		{
			name:    "empty list",
			inItems: []archiveItem{},
			inItem: archiveItem{
				sourcePath: "/foo/bar",
				header:     &cpio.Header{Name: "/foo/bar"},
			},
			expected: []archiveItem{
				{
					sourcePath: "/foo/bar",
					header:     &cpio.Header{Name: "/foo/bar"},
				},
			},
		},
		{
			name: "already exists",
			inItems: []archiveItem{
				{
					sourcePath: "/bazz/bar",
					header:     &cpio.Header{Name: "/bazz/bar"},
				},
				{
					sourcePath: "/foo",
					header:     &cpio.Header{Name: "/foo"},
				},
				{
					sourcePath: "/foo/bar",
					header:     &cpio.Header{Name: "/foo/bar"},
				},
			},
			inItem: archiveItem{
				sourcePath: "/foo",
				header:     &cpio.Header{Name: "/foo"},
			},
			expected: []archiveItem{
				{
					sourcePath: "/bazz/bar",
					header:     &cpio.Header{Name: "/bazz/bar"},
				},
				{
					sourcePath: "/foo",
					header:     &cpio.Header{Name: "/foo"},
				},
				{
					sourcePath: "/foo/bar",
					header:     &cpio.Header{Name: "/foo/bar"},
				},
			},
		},
		{
			name: "add new",
			inItems: []archiveItem{
				{
					sourcePath: "/bazz/bar",
					header:     &cpio.Header{Name: "/bazz/bar"},
				},
				{
					sourcePath: "/foo",
					header:     &cpio.Header{Name: "/foo"},
				},
				{
					sourcePath: "/foo/bar",
					header:     &cpio.Header{Name: "/foo/bar"},
				},
				{
					sourcePath: "/foo/bar1",
					header:     &cpio.Header{Name: "/foo/bar1"},
				},
			},
			inItem: archiveItem{
				sourcePath: "/foo/bar0",
				header:     &cpio.Header{Name: "/foo/bar0"},
			},
			expected: []archiveItem{
				{
					sourcePath: "/bazz/bar",
					header:     &cpio.Header{Name: "/bazz/bar"},
				},
				{
					sourcePath: "/foo",
					header:     &cpio.Header{Name: "/foo"},
				},
				{
					sourcePath: "/foo/bar",
					header:     &cpio.Header{Name: "/foo/bar"},
				},
				{
					sourcePath: "/foo/bar0",
					header:     &cpio.Header{Name: "/foo/bar0"},
				},
				{
					sourcePath: "/foo/bar1",
					header:     &cpio.Header{Name: "/foo/bar1"},
				},
			},
		},
		{
			name: "add new at beginning",
			inItems: []archiveItem{
				{
					sourcePath: "/foo",
					header:     &cpio.Header{Name: "/foo"},
				},
				{
					sourcePath: "/foo/bar",
					header:     &cpio.Header{Name: "/foo/bar"},
				},
			},
			inItem: archiveItem{
				sourcePath: "/bazz/bar",
				header:     &cpio.Header{Name: "/bazz/bar"},
			},
			expected: []archiveItem{
				{
					sourcePath: "/bazz/bar",
					header:     &cpio.Header{Name: "/bazz/bar"},
				},
				{
					sourcePath: "/foo",
					header:     &cpio.Header{Name: "/foo"},
				},
				{
					sourcePath: "/foo/bar",
					header:     &cpio.Header{Name: "/foo/bar"},
				},
			},
		},
		{
			name: "add new at end",
			inItems: []archiveItem{
				{
					sourcePath: "/bazz/bar",
					header:     &cpio.Header{Name: "/bazz/bar"},
				},
				{
					sourcePath: "/foo",
					header:     &cpio.Header{Name: "/foo"},
				},
			},
			inItem: archiveItem{
				sourcePath: "/zzz/bazz",
				header:     &cpio.Header{Name: "/zzz/bazz"},
			},
			expected: []archiveItem{
				{
					sourcePath: "/bazz/bar",
					header:     &cpio.Header{Name: "/bazz/bar"},
				},
				{
					sourcePath: "/foo",
					header:     &cpio.Header{Name: "/foo"},
				},
				{
					sourcePath: "/zzz/bazz",
					header:     &cpio.Header{Name: "/zzz/bazz"},
				},
			},
		},
	}

	for _, st := range subtests {
		t.Run(st.name, func(t *testing.T) {
			a := archiveItems{items: st.inItems}
			a.add(st.inItem)
			if !reflect.DeepEqual(st.expected, a.items) {
				t.Fatal("expected:", st.expected, " got: ", a.items)
			}
		})
	}
}

func TestExtractFormatLevel(t *testing.T) {
	tests := []struct {
		name           string
		in             string
		expectedFormat CompressFormat
		expectedLevel  CompressLevel
	}{
		{
			name:           "gzip, default level",
			in:             "gzip:default",
			expectedFormat: FormatGzip,
			expectedLevel:  LevelDefault,
		},
		{
			name:           "unknown format, level 12",
			in:             "pear:12",
			expectedFormat: FormatGzip,
			expectedLevel:  LevelDefault,
		},
		{
			name:           "zstd, level not given",
			in:             "zstd",
			expectedFormat: FormatZstd,
			expectedLevel:  LevelDefault,
		},
		{
			name:           "zstd, invalid level 'fast:'",
			in:             "zstd:fast:",
			expectedFormat: FormatZstd,
			expectedLevel:  LevelDefault,
		},
		{
			name:           "zstd, best",
			in:             "zstd:best",
			expectedFormat: FormatZstd,
			expectedLevel:  LevelBest,
		},
		{
			name:           "zstd, level empty :",
			in:             "zstd:",
			expectedFormat: FormatZstd,
			expectedLevel:  LevelDefault,
		},
		{
			name:           "gzip, best",
			in:             "gzip:best",
			expectedFormat: FormatGzip,
			expectedLevel:  LevelBest,
		},
		{
			name:           "<empty>, <empty>",
			in:             "",
			expectedFormat: FormatGzip,
			expectedLevel:  LevelDefault,
		},
		{
			name:           "lzma, fast",
			in:             "lzma:fast",
			expectedFormat: FormatLzma,
			expectedLevel:  LevelDefault,
		},
		{
			name:           "lz4l, fast",
			in:             "lz4l:fast",
			expectedFormat: FormatLz4,
			expectedLevel:  LevelFast,
		},
		{
			name:           "none",
			in:             "none",
			expectedFormat: FormatNone,
			expectedLevel:  LevelDefault,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			format, level := ExtractFormatLevel(test.in)
			if format != test.expectedFormat {
				t.Fatal("format expected: ", test.expectedFormat, " got: ", format)
			}
			if level != test.expectedLevel {
				t.Fatal("level expected: ", test.expectedLevel, " got: ", level)
			}

		})
	}
}
